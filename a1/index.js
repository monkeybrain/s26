const http = require('http');

const PORT = 3000;

http.createServer(function(request, response){

if(request.url == '/login'){
	response.writeHead(200, {'content-Type': 'text/plain'})
	response.end('You are in the login page')
} else {
		response.writeHead(404, {'content-Type': 'text/plain'})
	response.end('This is an error message, wrong page. Get out bye!')
}
}).listen(PORT);

console.log(`Server is running at localhost: ${PORT}`)