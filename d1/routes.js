const http = require('http');

const PORT = 4000;

http.createServer(function(request, response){

if(request.url == '/hello'){
	response.writeHead(200, {'content-Type': 'text/plain'})
	response.end('Hello of the World')
} else {
		response.writeHead(404, {'content-Type': 'text/plain'})
	response.end('End of the World')
}
}).listen(PORT);

console.log(`Server is running at localhost: ${PORT}`)